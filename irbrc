require 'irb/ext/save-history'
IRB.conf[:SAVE_HISTORY] = 10_000
IRB.conf[:HISTORY_FILE] = "#{ENV['HOME']}/.irb-save-history"

def load_factory_girl
  require 'factory_girl'
  Dir["spec/factories/**.rb"].each{|f| require_relative f}
end
