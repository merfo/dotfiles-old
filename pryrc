def load_factory_girl
  require 'factory_girl'
  Dir["spec/factories/**.rb"].each{|f| require "./#{f}"}
end
