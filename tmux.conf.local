bind-key -r n next-window
bind-key -r p previous-window

# toggle zoom pane
bind-key -r _ resize-pane -Z

# enable nested tmux prefix C-a
bind-key a send-prefix

unbind C-b
set -g prefix C-a
bind C-a send-prefix

# screen like toggling last two windows
bind-key C-a last-window

# hostname is printed on the left, uptime and current load on the right
set -g status-left "#S"
set -g status-right ""
#set -g status-right "#(uptime|cut -d "," -f 2-)"

# statusbar background to black, foreground to white
set-option -g status-fg white
set-option -g status-bg black

set-option -g message-fg blue
set-option -g message-bg black

# set the current window name to a nice bold yellow text
setw -g window-status-current-attr bold
setw -g window-status-current-fg '#bb3388'

# be notified when there is activity in one of your windows
setw -g monitor-activity on

# reload changes in tmux, handy
bind r source-file ~/.tmux.conf

# scrollback
set -g history-limit 10000

# bind Ctrl-Alt-(left|right) to switch windows
bind-key -n M-C-left prev
bind-key -n M-C-right next

bind-key -n S-M-left select-pane -L
bind-key -n S-M-right select-pane -R
bind-key -n S-M-up select-pane -U
bind-key -n S-M-down select-pane -D

bind-key -n S-M-h select-pane -L
bind-key -n S-M-l select-pane -R
bind-key -n S-M-j select-pane -U
bind-key -n S-M-k select-pane -D

bind-key S command-prompt -p "Make/attach session:" "new-window 'tmuxstart \'%%\'""'"

bind-key Space choose-session

set -g base-index 0
set-window-option -g pane-base-index 0

set-option -g default-command "reattach-to-user-namespace -l zsh"

# Setup 'v' to begin selection as in Vim
bind-key -t vi-copy v begin-selection
bind-key -t vi-copy y copy-pipe "reattach-to-user-namespace pbcopy"

# Update default binding of `Enter` to also use copy-pipe
unbind -t vi-copy Enter
bind-key -t vi-copy Enter copy-pipe "reattach-to-user-namespace pbcopy"

set-window-option -g window-status-fg colour244
set-window-option -g window-status-bg default

bind-key -n C-, previous-window
bind-key -n C-. next-window

if-shell "[ -f ~/.tmux.conf.colors ]" 'source ~/.tmux.conf.colors'

# List of plugins
# # Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
#run '~/.tmux/plugins/tpm/tpm'
